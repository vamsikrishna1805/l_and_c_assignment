import java.util.*;
import java.util.Random;
public class GuessANumber{
    static boolean checkNumberForRange(int number)
    {
        if(number >= 1 && number <= 100)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

     public static void main(String[] args){
         Random rand=new Random();
        int randomNumber = rand.nextInt((100-1)+1)+1;
        boolean isGuessedNumberCorrect = false;
        Scanner inputFromUser = new Scanner(System.in);
        int guessedNumber;
        System.out.println("Guess a number between 1 and 100");
        guessedNumber = inputFromUser.nextInt();
         int attempts = 0;
        while(!isGuessedNumberCorrect)
        {
            if(!checkNumberForRange(guessedNumber))
            {
                System.out.println("Invalid number,Please guess and enter a number between 1 and 100");
                guessedNumber = inputFromUser.nextInt();
                continue;
            }
            else
            {
                attempts += 1;
            }
            if(guessedNumber < randomNumber)
            {
                System.out.println("Guessed number is too low, Guess and enter number again");
                guessedNumber = inputFromUser.nextInt();
            }
            else if(guessedNumber > randomNumber)
            {
                System.out.println("Guessed number is too high, Guess and enter number again");
                guessedNumber = inputFromUser.nextInt();
            }
            else
            {
                System.out.println("You guessed it in:" +attempts + " guesses");
                isGuessedNumberCorrect = true;
            }
        }
     }
}