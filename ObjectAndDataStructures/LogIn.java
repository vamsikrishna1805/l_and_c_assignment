import java.util.*;
 
interface LogInFunctionality {
    public void logInType();
}
 
class LogInWithUsernameAndPassword implements LogInFunctionality {
    public void logInType() {
        System.out.println("LogInWithUsernameAndPassword");
    }
}

 
class LogInWithOTP implements LogInFunctionality {
    public void logInType() {
        System.out.println("LogInWithOTP");
    }
}
 
class LogInFactory {
    public LogInFunctionality getLogInType() {
        if (selectLogInType()) {
            return new LogInWithUsernameAndPassword();
        } 
        else {
            return new LogInWithOTP();
        }
    }

    private boolean selectLogInType() {
        int logInChoice;
        System.out.println("Select Type of LogIn \n Make Choice 1 or 2:");
        System.out.println("1.LogIn with Username and Password");
        System.out.println("2.LogIn with OTP");
        Scanner inputFromUser = new Scanner(System.in);
        logInChoice = inputFromUser.nextInt();
        switch(logInChoice) {
            case 1: return true;
            case 2: return false;
            default: System.out.println("Make choice correctly again:");
        }
        return true;
    }
}
 
class LogIn {
    public static void main(String args[]) {
        LogInFactory product = new LogInFactory();
        LogInFunctionality logInTypeObj = product.getLogInType();
        logInTypeObj.logInType();
    }
}