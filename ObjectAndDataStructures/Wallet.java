import java.util.*;

public class Wallet { 
    private float value = 10.00f; 
    public float getTotalMoney() { 
        return value; 
    }
    public void setTotalMoney(float newValue) { 
        value = newValue; 
        } 
    public void addMoney(float deposit) {
            value += deposit;
        } 
    public void subtractMoney(float debit) { 
        value -= debit; 
        } 
} 