import java.util.*;

public class Customer {
    private String firstName = "vamsi";
    private String lastName = "sivakavi";
    private static String username;
    private static String password;
    private static String phoneno;
    private Wallet myWallet;
    //Customer c;
    public static Boolean alreadyUser = false;
    public String getFirstName(){
        return firstName;
    }
    public String getLastName(){
        return lastName;
    }
    public static void setUsername(String un){
        Customer.username = un;
    }
    public static void setPassword(String pwd){
        Customer.password = pwd;
    }
    public static void setPhoneno(String phno){
        Customer.phoneno = phno;
    }
    public static String getUserName(){
        return Customer.username;
    }
    public static String getPassword(){
        return Customer.password;
    }
    public static String getPhoneno(){
        return Customer.phoneno;
    }

    public float getPayment(float bill) {
        //c = new Customer()
        Scanner sc = new Scanner(System.in);
        String status;
        if(Customer.alreadyUser == true){
            status = Customer.logIn();
            System.out.println(status);
        }
        else{
            System.out.println(Customer.signUp());
            status = Customer.logIn();
            System.out.println(status);
        }
        if(status == "Login Successful"){
            System.out.println("Enter your registered number to get OTP:");
            String phno = sc.nextLine();
            if(phno.equals(Customer.getPhoneno())){
                int otp = Customer.getOTP();
                System.out.println(otp);
                System.out.println("Enter OTP you got to logIn to your Wallet:");
                int userOTP = sc.nextInt();
                if(otp == userOTP){
                    System.out.println("Correct OTP, Using your wallet to pay paper bill");
                    myWallet = new Wallet();
                }
                else{
                    System.out.println("Incorrect OTP");
                    System.out.println("Unable to open wallet");
                    System.exit(0);
                }
            }
            else{
                System.out.println("Phone number has not registered");
                System.exit(0);
            }
            //System.out.println("Try Again!");
            //status = login();
        }
        else{
            System.out.println("Unable to open wallet");
            System.exit(0);
        }
        //Authenticator.setDefault(new BasicAuthenticator(getUserName(), getPassword()));
        //System.out.println(logIn());
        //myWallet = new Wallet();
        float payment = 0.00f;
        if (myWallet != null) {
            if (myWallet.getTotalMoney() > bill) {
                myWallet.subtractMoney(bill); 
                payment = bill;
            } 
        }
        return payment;
    }

    public static String signUp() {
        Scanner sc = new Scanner(System.in);
        Customer.alreadyUser = true;
        String username;
        String password;
        String phoneno;
        System.out.println("SignUP");
        System.out.println("Enter User Name:");
        username = sc.nextLine();
        Customer.setUsername(username);
        System.out.println("Enter Password:");
        password = sc.nextLine();
        Customer.setPassword(password);
        System.out.println("Enter your phone number:");
        phoneno = sc.nextLine();
        Customer.setPhoneno(phoneno);
        return "SignUp Successful";
    }

    public static String logIn(){
        Scanner sc = new Scanner(System.in);
        String username;
        String password;
        String phoneno;
        System.out.println("SignIn");
        System.out.println("Enter User Name:");
        username = sc.nextLine();
        System.out.println("Enter Password:");
        password = sc.nextLine();
        if(username.equals(Customer.getUserName()) && password.equals(Customer.getPassword())){
            return "Login Successful";
        }
        else {
            return "invalid username or password";
        }
    }
    
    public static int getOTP(){
        Random rand = new Random();
        int OTP = rand.nextInt(10000);
        return OTP;
    }
}
