import java.util.*;

public class Paperboy {
    public static void getBillAmount() {
        Customer myCustomer = new Customer();
        float payment = 2.00f; // “I want my two dollars!” 
        float paidAmount = myCustomer.getPayment(payment); 
        if(paidAmount == payment) {
        // say thank you and give customer a receipt
        System.out.println("say thank you and give customer a receipt");
        } 
        else {
            // come back later and get my money
            System.out.println("come back later and get my money");
        }
    }
    public static void main(String args[]) {
        getBillAmount();
    }
}