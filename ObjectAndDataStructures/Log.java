import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Properties;
 
interface Logger
{
  public void logMessage(String message);
}
 
class FileLogger implements Logger
{
  public void logMessage(String message)
  {
    try
    {
      File file =new File("Filelog.log");
 
      //create file if doesn't exist 
      if(!file.exists())
      {
        file.createNewFile();
      }
 
      //true = append file
      FileWriter fileWritter = new FileWriter(file.getName(),true);
      BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
      bufferWritter.write(message);
      bufferWritter.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
    System.out.println("Message logged in file: " + message);
  }
}
 
class ConsoleLogger implements Logger
{
  public void logMessage(String message)
  {
    System.out.println("Console: " + message);
  }
}
 
class Factory
{
  public Logger getLogger()
  {
    if (isFileLoggingEnabled()) 
    {
      return new FileLogger();
    } 
    else 
    {
      return new ConsoleLogger();
    }
  }
 
  // helper method, check if FileLogging is ON
  // if so, log message to a file else print it to console.
  private boolean isFileLoggingEnabled() 
  {
    Properties p = new Properties();
    try 
    {
      p.load(ClassLoader.getSystemResourceAsStream("logger.properties"));
      String fileLoggingValue = p.getProperty("FileLogging");
      if (fileLoggingValue.equalsIgnoreCase("ON") == true)
        return true;
      else
        return false;
	} 
    catch (IOException e) 
    {
      return false;
    }
  }
}
 
class FactoryMethodPattern
{
  public static void main(String args[])
  {
    Factory f = new Factory();
    Logger msgLogger = f.getLogger();
    msgLogger.logMessage("Sample log message" + "\n");
  }
}