import java.util.*;
import random;
public class Dice{
    
    int getOutcomeOfDice(int sizeOfTheDice)
    {
        int diceValue = random.randInt(1,sizeOfTheDice);
        return diceValue;
    }

     public static void main(String []args){
        int facesOfTheDice = 6;
        boolean rollADice = true;
        char quitRollingADice;
        int diceOutcome;
        Scanner inputFromConsole = new Scanner(System.in);
        while(RollADice)
        {
            System.out.println("If you want to quit rolling a dice then press q");
            quitRollingADice = inputFromConsole.nextChar();
            if(quitRollingADice != "q")
            {
                diceOutcome = getOutcomeOfDice(facesOfTheSize);
                System.out.println("The result after rolling a dice is:" + diceOutcome);
            }
            else
            {
                rollADice = false;
            }
        }
     }
}