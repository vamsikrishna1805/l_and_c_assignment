//
//  TransformationOfData.swift
//  Tumblr
//
//  Created by Vamsi Krishna on 15/04/20.
//  Copyright © 2020 In Time tec. All rights reserved.
//

import Cocoa

class TransformationOfData: NSObject {
    var dataFromurl = Data()
    func getDataFromURL() -> Data {
        var request = URLRequest(url: URL(string: tumblrURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //Using get method to get data
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          var tumblerString = String(decoding: data, as: UTF8.self)
            //Coverting whole data as proper JSON String by eliminating some string
            while(tumblerString.starts(with: "var tumblr_api_read = ")) {
                tumblerString = tumblerString.replacingOccurrences(of: "var tumblr_api_read = ", with: "", options: [.caseInsensitive, .regularExpression])
            }
            tumblerString = tumblerString.replacingOccurrences(of: ";", with: "", options: [.caseInsensitive, .regularExpression])
            self.dataFromurl = Data(tumblerString.utf8)
          semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        return dataFromurl
    }
    
    func printReceviedData(dataFromURL: Data) {
        //JSONSerialization for decoding the data
        let json = try? JSONSerialization.jsonObject(with: dataFromURL, options: [])
        //Going through the contents what we want from API data
        if let dictionary = json as? [String: Any] {
            if let nestedDictionary = dictionary["tumblelog"] as? [String: Any] {
                if let title = nestedDictionary["title"] as? String {
                    print("Title: \(title)")
                }
                if let name = nestedDictionary["name"] as? String {
                    print("Name: \(name)")
                }
                if let description = nestedDictionary["description"] as? String {
                    print("Description: \(description)")
                }
            }
            if let noOfPosts = dictionary["posts-total"] as? Int {
                print("Number of posts: \(noOfPosts)")
            }
            if let posts = dictionary["posts"] as? [[String: Any]] {
                var count = 0
                for i in posts {
                   if let urls = i["photo-url-1280"] as? String {
                        count += 1
                    print("\(count). \(urls)")
                    }
                }
            }
        }
    }
}
