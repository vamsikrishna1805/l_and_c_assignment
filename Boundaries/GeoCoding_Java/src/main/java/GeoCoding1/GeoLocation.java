package GeoCoding1;

import java.util.Scanner;

import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageForwardRequest;
import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.byteowls.jopencage.model.JOpenCageResponse;

public class GeoLocation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String locationAddressName;
		System.out.println("Enter the location name to get latitude and longitude");
		Scanner userInput = new Scanner(System.in);
		locationAddressName = userInput.next();
		JOpenCageLatLng locationCoordinates;
		locationCoordinates= getLocationCoordinates(locationAddressName);
		try {
			System.out.println(locationCoordinates.getLat());
			System.out.println(locationCoordinates.getLng());
		}
		catch(Exception e) {
			System.out.println("Invalid input address,please give valid address next time");
		}
		//System.out.println("Hi");
	}
	
	public static JOpenCageLatLng getLocationCoordinates(String locationName) {
		JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder("3eb56252b50a40fbb86577fb0597c7e4");
        JOpenCageForwardRequest request = new JOpenCageForwardRequest(locationName);
        //request.setRestrictToCountryCode("za"); // restrict results to a specific country
        //request.setBounds(18.367, -34.109, 18.770, -33.704); // restrict results to a geographic bounding box (southWestLng, southWestLat, northEastLng, northEastLat)

        JOpenCageResponse response = jOpenCageGeocoder.forward(request);
        JOpenCageLatLng firstResultLatLng = response.getFirstPosition(); // get the coordinate pair of the first result
        //System.out.println(firstResultLatLng.getLat());
        //System.out.println(firstResultLatLng.getLng());
		return firstResultLatLng;
	}

}
