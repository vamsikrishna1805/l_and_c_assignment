package GeoCoding1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.byteowls.jopencage.model.JOpenCageLatLng;

public class LocationCoordinatesTest {
	
	@Test
	public void testLocationCoordinates() {
		double lat;
		double lng;
		JOpenCageLatLng locationCoordinates;
		locationCoordinates = GeoLocation.getLocationCoordinates("Bengaluru");
		lat = locationCoordinates.getLat();
		lng = locationCoordinates.getLng();
		assertEquals(12.9749677, lat, 25.9499354);
		assertEquals(77.6019481, lng, 155.203896);
	}
}
