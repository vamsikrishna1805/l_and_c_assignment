//
//  main.swift
//  Geocoding
//
//  Created by Vamsi Krishna on 15/07/20.
//  Copyright © 2020 In Time tec. All rights reserved.
//

import Foundation

var semaphore = DispatchSemaphore (value: 0)
print("Hello, World!")
let geocodingAPI = GeocodingAPI()
geocodingAPI.getLocationNameFromUser()


let googleAPI: GoogleAPI = GoogleAPI()
let openCageDataAPI: OpenCageDataAPI = OpenCageDataAPI()

var adapter: APIAdapter = APIAdapter(openCageDataAPI)
let isGeoDataAvailable = adapter.getData()
if !(isGeoDataAvailable) {
    adapter = APIAdapter(googleAPI)
}
else {
    let latitude = adapter.getLatitude()
    let longitude = adapter.getLongitude()
    print(latitude)
    print(longitude)
}

