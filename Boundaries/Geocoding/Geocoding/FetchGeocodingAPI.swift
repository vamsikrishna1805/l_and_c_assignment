//
//  FetchGeocodingAPI.swift
//  Geocoding
//
//  Created by Vamsi Krishna on 16/07/20.
//  Copyright © 2020 In Time tec. All rights reserved.
//

import Foundation

protocol FetchGeocodingAPI {
    
    func selectURLAPI() -> Void
    func getDataFromAPI() -> Data?
    func getLocationCoordinatesFromGeoData(_ geoData: Data) -> Void
    func getLatitude() -> Double
    func getLongitude() -> Double
}

class GoogleAPI : FetchGeocodingAPI {
    var latitude: Double?
    var longitude: Double?
    
    
    func selectURLAPI() {
        print("Google API")
    }
    
    func getDataFromAPI() -> Data? {
        print("Google API")
        return nil
    }
    
    func getLocationCoordinatesFromGeoData(_ geoData: Data) {
        print("Google API")
    }
    
    func getLatitude() -> Double{
        print("Google API")
        return 0.0
    }
    
    func getLongitude() -> Double {
        print("Google API")
        return 0.0
    }
    
}

class OpenCageDataAPI : FetchGeocodingAPI {
    
    var latitude: Double?
    var longitude: Double?
    var dataFromAPI : Data? = nil
    var locationName : String? = nil
    var APIURL : String {
        get {
            return "https://api.opencagedata.com/geocode/v1/json?q=\(locationName ?? "Bengaluru")&key=3eb56252b50a40fbb86577fb0597c7e4&language=en&pretty=1"
        }
    }
    
    func selectURLAPI() {
        print("Enter the location name to get latitude and longitude")
        locationName = readLine()!
        locationName = locationName!.replacingOccurrences(of: " ", with: "", options: [.caseInsensitive, .regularExpression])
        locationName = locationName!.replacingOccurrences(of: "&", with: "", options: [.caseInsensitive, .regularExpression])
    }
    
    func getDataFromAPI() -> Data? {
        var request = URLRequest(url: URL(string: APIURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //Using get method to get data
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            self.dataFromAPI = data
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        return dataFromAPI
    }
    
    func getLocationCoordinatesFromGeoData(_ geoData: Data) {
        //JSONSerialization for decoding the data
        let json = try? JSONSerialization.jsonObject(with: geoData, options: [])
        //Going through the contents what we want from API data
        if let dictionary = json as? [String: Any] {
            if let nestedresults = dictionary["results"] as? [[String: Any]] {
                if(nestedresults.count > 0) {
                    if let geometry = nestedresults[0] ["geometry"] as? [String: Any] {
                        if let lat = geometry["lat"] as? Double {
                            latitude = lat
                            print("Latitude:\(lat)")
                        }
                        if let lng = geometry["lng"] as? Double {
                            longitude = lng
                            print("Longitude:\(lng)")
                        }
                    }
                }
                else {
                    print("Invalid input location,please give valid address next time")
                }
            }
        }
    }
    
    func getLatitude() -> Double {
        return latitude!
    }
    
    func getLongitude() -> Double{
        return longitude!
    }
}

class APIAdapter {
    
    var fetchGeocodingAPI: FetchGeocodingAPI
    
    init(_ fetchGeocodingAPI: FetchGeocodingAPI) {
        self.fetchGeocodingAPI = fetchGeocodingAPI
    }
    
    func getData() -> Bool {
        if fetchGeocodingAPI is GoogleAPI {
            fetchGeocodingAPI.selectURLAPI()
            let geoData = fetchGeocodingAPI.getDataFromAPI()
            if geoData == nil {
                print("The data is unavailable, try with another API")
                return false
            }
            else {
                fetchGeocodingAPI.getLocationCoordinatesFromGeoData(geoData!)
                return true
            }
        }
        if fetchGeocodingAPI is OpenCageDataAPI {
            fetchGeocodingAPI.selectURLAPI()
            let geoData = fetchGeocodingAPI.getDataFromAPI()
            if geoData == nil {
                print("The data is unavailable, try with another API")
                return false
            }
            else {
                fetchGeocodingAPI.getLocationCoordinatesFromGeoData(geoData!)
                return true
            }
        }
        return true
    }
    
    func getLatitude() -> Double {
        return fetchGeocodingAPI.getLatitude()
    }
       
    func getLongitude() -> Double {
        return fetchGeocodingAPI.getLongitude()
    }
    
}
