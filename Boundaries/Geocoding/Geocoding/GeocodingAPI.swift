//
//  GeocodingAPI.swift
//  Geocoding
//
//  Created by Vamsi Krishna on 15/07/20.
//  Copyright © 2020 In Time tec. All rights reserved.
//

import Cocoa

class GeocodingAPI: NSObject {
    
    var latitude : Double? = nil
    var longitude : Double? = nil
    var locationName : String? = nil
    var geoData : Data? = nil
    var APIURL : String {
        get {
            return "https://api.opencagedata.com/geocode/v1/json?q=\(locationName ?? "Bengaluru")&key=3eb56252b50a40fbb86577fb0597c7e4&language=en&pretty=1"
        }
    }
    var dataFromAPI = Data()
    
    func getLocationNameFromUser() {
        print("Enter the location name to get latitude and longitude")
        locationName = readLine()!
        locationName = locationName!.replacingOccurrences(of: " ", with: "", options: [.caseInsensitive, .regularExpression])
        locationName = locationName!.replacingOccurrences(of: "&", with: "", options: [.caseInsensitive, .regularExpression])
        getDataFromAPI(locationName: locationName)
    }
    
    func getDataFromAPI(locationName: String?) {
        var request = URLRequest(url: URL(string: APIURL)!,timeoutInterval: Double.infinity)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        //Using get method to get data
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            self.dataFromAPI = data
            self.getLocationCoordinatesFromGeoData(geoData: self.dataFromAPI)
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
    }
    
    func getLocationCoordinatesFromGeoData(geoData: Data) {
        //JSONSerialization for decoding the data
        let json = try? JSONSerialization.jsonObject(with: geoData, options: [])
        //Going through the contents what we want from API data
        if let dictionary = json as? [String: Any] {
            if let nestedresults = dictionary["results"] as? [[String: Any]] {
                if(nestedresults.count > 0) {
                    if let geometry = nestedresults[0] ["geometry"] as? [String: Any] {
                        if let lat = geometry["lat"] as? Double {
                            latitude = lat
                            print("Latitude:\(lat)")
                        }
                        if let lng = geometry["lng"] as? Double {
                            longitude = lng
                            print("Longitude:\(lng)")
                        }
                    }
                }
                else {
                    print("Invalid input location,please give valid address next time")
                }
            }
        }
    }
    
    func getLatitude() -> Double {
        return latitude!
    }
    
    func getLongitude() -> Double {
        return longitude!
    }
    
}
