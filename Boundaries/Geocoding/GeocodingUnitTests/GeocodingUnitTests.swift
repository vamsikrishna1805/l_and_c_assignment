//
//  GeocodingUnitTests.swift
//  GeocodingUnitTests
//
//  Created by Vamsi Krishna on 24/07/20.
//  Copyright © 2020 In Time tec. All rights reserved.
//

//https://developer.apple.com/forums/thread/52211
import XCTest
@testable import Geocoding

class GeocodingUnitTests: XCTestCase {
    var geocodingAPITest : GeocodingAPI!

    override func setUpWithError() throws {
        super.setUp()
        geocodingAPITest = GeocodingAPI()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        super.tearDown()
        geocodingAPITest = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLocationCoordinates() {
        geocodingAPITest.getDataFromAPI(locationName: "Bengaluru")
        let latitude = geocodingAPITest.getLatitude()
        let longitude = geocodingAPITest.getLongitude()
        XCTAssertEqual(latitude, 12.97911980)
        XCTAssertEqual(longitude, 77.5912997)
    }

}
