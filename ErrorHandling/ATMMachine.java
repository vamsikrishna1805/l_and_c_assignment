import java.util.*;
import java.net.HttpURLConnection; 
import java.net.URL;
import java.io.OutputStream;

public class ATMMachine {
    int PIN;
    int attemptCount = 1;
    float amountInATM = 10000.0f;
    BankAccount ba = new BankAccount();

    public void authentication() throws CardBlockException {
        System.out.println("Please Insert your card");
        System.out.println("Please Enter your PIN number");
        Scanner input = new Scanner(System.in);
        PIN = input.nextInt();
        do {
            attemptCount += 1;
            if(PIN == ba.getPINNumber()) {
                connectToBankServer();
                transaction();
                System.exit(1);
            }
            else {
                System.out.println("The PIN number you have entered is wrong");
                System.out.println("Try again one more time");
                PIN = input.nextInt();
                if(attemptCount == 3) {
                    throw new CardBlockException("You card is blocked Try again after 24 hours or contact bank");
                }
            }
        }while(attemptCount < 3);
    }

    public void transaction(){
        int choice;
        System.out.println("Please select an option");
        System.out.println("1. Withdraw");
        System.out.println("2. Deposit");
        System.out.println("3. Balance");
        Scanner input = new Scanner(System.in);
        choice = input.nextInt();
        switch(choice) {
            case 1 : 
            try {
                withdrawAmount();
            }
            catch(Exception e) {
                System.out.println(e);
            }
            break;
            default:System.out.println("Wrong choice");
        }
    }

    public void withdrawAmount() throws InsufficientAccountBalanceException, InsufficientFundsInATMException {
        float amountInAccount = ba.getAccountBalance();
        float withdrawingAmount;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter amount to withdraw");
        withdrawingAmount = input.nextFloat();
        if(withdrawingAmount <= amountInATM) {
            if(withdrawingAmount <= amountInAccount && withdrawingAmount > 0.0) {
                float remainingBalance = amountInAccount - withdrawingAmount;
                ba.setAccountBalance(remainingBalance);
                System.out.println("You have withdrawn amount: "+withdrawingAmount+" and your new balance is: "+remainingBalance);
            }
            else {
                throw new InsufficientAccountBalanceException("Insufficient funds in your account");
            }
        }
        else {
           throw new InsufficientFundsInATMException("Insufficient funds in ATM");
        }
    }

    public static void main(String args[]) {
        ATMMachine atm = new ATMMachine();
        try {
            atm.authentication();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }
    public void connectToBankServer() {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String jsonbody = "{\"name\":\"sonoo\",\"salary\":600000.0,\"age\":27}";
        String result = "";
        try {
            url = new URL("http://localhost:3000/Data");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            OutputStream wr = urlConnection.getOutputStream();
            wr.write(jsonbody.getBytes());
            wr.flush();
            wr.close();
            System.out.println("response code from server" + urlConnection.getResponseCode());
            //return urlConnection.getResponseCode();
        } 
        catch (Exception e) {
            System.out.println(e);
            //e.printStackTrace();
            //return 500;
        }
        finally {
            urlConnection.disconnect();
        }
    }
}