import java.util.*;
import java.lang.Exception;

public class InsufficientAccountBalanceException extends Exception {
    public InsufficientAccountBalanceException(String message) {
        super(message);
    }
}