import java.util.*;
import java.lang.Exception;

public class InsufficientFundsInATMException extends Exception {
    public InsufficientFundsInATMException(String message) {
        super(message);
    }
}