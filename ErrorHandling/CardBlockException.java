import java.util.*;
import java.lang.Exception;

public class CardBlockException extends Exception {

    public CardBlockException(String message) {
        super(message);
    }
}