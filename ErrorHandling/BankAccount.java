import java.util.*;

public class BankAccount {
    private int PINNumber = 1234;
    private float balance = 5000.0f;

    public int getPINNumber() {
        return PINNumber;
    }

    public void setAccountBalance(float remainingBalance) {
        balance = remainingBalance;
    }
    
    public float getAccountBalance() {
        return balance;
    }
}